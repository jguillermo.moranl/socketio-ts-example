
import { Request, Response, NextFunction } from 'express'
import { HandleErrorsMongose } from '../libs/handleErrorsMongoDB'
import error from '../libs/errors'
import Example from '../models/example.models'
import { Iexample } from '../interfaces/Iexample.interfaces';

export class ExampleController {

    push_on_collection_example = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const example: Iexample = req.body as Iexample
            /* 
            Transformar en metodo pre-save 
            investigar "this"    
             if (typeof new_password === 'boolean') {
                     throw new error('Error in create user.', 'Errors validations', 403, 'Register Controllers', [{ message: 'Min length of characters is 8.. pls try again..', key: 'password' }])
                 } */
            /*             user.password = new_password */

            const save_example = await Example.create(example)


            res.status(200).json({ save_example })

        } catch (e) {
            /** generar logica de mensajes, para n errores y un error */
            if (e.errors) {
                const mensaje = HandleErrorsMongose(e.errors)
                next(new error('Error in create user.', 'Errors validations', 403, 'Register Controllers', mensaje))
            }
            next(e)
        }
        /* res.json({ user }) */
    }
}