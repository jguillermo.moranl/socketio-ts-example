let PORT: Number = 0
let DB_NAME: String = ''
let DB_CONNECTION: String = ''

PORT = Number(process.env.PORT) || 3000
DB_NAME = process.env.DB_NAME || 'dbexample'
DB_CONNECTION = process.env.DB_CONNECTION || 'mongodb://localhost'
export { PORT, DB_NAME, DB_CONNECTION }