/**
 * DESCRIPCIÓN DIRECTORIO SRC:
 * src /
 *      Controllers/:Todos los archivos que se encargaran de orquestar la aplicación
 *      lib/: Todos los archivos con codigo compartido entre la aplicación
 *      models/: Todos los archivos de modelos de la aplicación
 *      routes/: Todos los archivos de rutas de la aplicación
 *      views/: Todas las vistas de la aplicacíon
 *      server/: Archivos de servidor.
 *      index.ts : Punto de entrada de la aplicación
 *
 * tips:
 *      npm install @types/express ayuda al tipado de typescripte para el framework express
 *
 * Comandos configurados:
 *      npm run clean: (rm || del in windows -rf build) comando para eliminar la carpeta build que genera tsc 'typescriptcompiler'
 */

const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http, {
  cors: {
    origin: "http://localhost:4200",
    methods: ["GET", "POST"],
  },
});

io.on("connection", (socket: any) => {
  console.log("a user connected");
  io.emit("MESSAGE_FROM_BACK", "DESDE BACKEND EL LEPE SE LA COME");
  io.emit("otro_evento", "OTRO EVENTO DESDE BACK");
  io.on("SEND_FROM_FRONT", (data: any) => console.log(data));
});

http.listen(3000, () => {
  console.log("listening on *:3000");
});
