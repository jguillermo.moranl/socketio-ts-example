import { Document } from "mongoose";

export interface Iexample {
    name: string;
    last_name: string;
    alias: string;
    user: string;
    password: string;
}

export interface IMexample extends Document {
    name: string;
    last_name: string;
    alias: string;
    user: string;
    password: string;
}