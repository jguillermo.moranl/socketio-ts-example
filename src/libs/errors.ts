export default class HandleError extends Error {
    status: number;
    from: string;
    messages: any;
    constructor(name: string, message: string, status: number, from: string, messages?: any) {
        super(message);
        this.name = name;
        this.message = message;
        this.messages = messages;
        this.status = status;
        this.from = from;
    }

}