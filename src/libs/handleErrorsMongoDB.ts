
interface Ierrors {
    message: string[];
    key: string;
}

export function HandleErrorsMongose(errores: any): Ierrors[] {
    const mensaje: Ierrors[] = []
    for (let key in errores) {
        mensaje.push({ message: errores[key].message, key });
    }
    return mensaje;
}