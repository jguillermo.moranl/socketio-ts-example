import { Request, Response, NextFunction } from 'express';
import HandleError from '../libs/errors';

function ExceptionError(error: HandleError, req: Request, res: Response, next: NextFunction) {
    res.status(error.status || 500).json({
        name: error.name,
        message: error.message,
        group_messages: (error.messages) ? error.messages : [],
        from: error.from
    });
    next();
}

export default ExceptionError;

