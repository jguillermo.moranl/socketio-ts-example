/** Model Users */
import { Schema, model } from 'mongoose'
import { IMexample } from '../interfaces/Iexample.interfaces';

const example_schema: Schema<IMexample> = new Schema(
    {
        name: { type: String, minlength: [3, 'ho is u name??... (min 3 characters)'] },
        last_name: { type: String, minlength: [3, 'ho is u last name??... (min 3 characters)'] },
        alias: { type: String, minlength: [2, 'it could be something like lazy bob (min 2 characters)'], unique: [true, 'this alias is asignate of the other member.. try again pls!'] },
        user: { type: String, required: [true, 'tell us your email pls.. friend!'] },
        password: { type: String, required: [true, 'just something u should know...'], minlength: [32, 'error.. password hash invalid'] }
    }
)


export default model<IMexample>('example', example_schema)