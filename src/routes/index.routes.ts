import express from 'express'
import example_routes from './test-conection/testconection.routes'
const app = express()

app.use('/example', example_routes)
export default app