import { Router } from 'express';

import { ExampleController } from '../../controllers/example.controller'

const router: Router = Router();

const co_ex = new ExampleController();

router.post('/hello', co_ex.push_on_collection_example);

export default router;