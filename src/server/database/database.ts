import mongoose = require('mongoose')
import { DB_NAME, DB_CONNECTION } from '../../environment/environment'

const uri = `${DB_CONNECTION}/${DB_NAME}`;

export default class DataBaseConection {
    static instance_mongoose: any;
    static conexion_mongoose: mongoose.Connection;

    constructor() {
        DataBaseConection.init_bd();
    }

    static init_bd() {
        if (this.instance_mongoose) {
            return this.instance_mongoose;
        } else {
            this.instance_mongoose = mongoose.connect(uri, {
                useNewUrlParser: true,
                useCreateIndex: true
            })
                .then(db => {
                    console.log({ DB_CONNECTION }, { DB_NAME });

                    console.log('Conexión realizada.');
                })
                .catch(error => {
                    console.error('Error conexión a base de datos.');
                    console.error('Error:', error);
                });
        }

    }
}