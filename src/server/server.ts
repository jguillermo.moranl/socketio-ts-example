import express from "express";
/* import expres_handlebars from 'express-handlebars';
import path from 'path'; */
import database from "./database/database";

export default class Server {
  app: express.Application;
  port: Number;
  database: any;

  constructor(PORT: Number) {
    this.port = PORT;
    this.app = express();
    // this.database = new database();
  }

  static init_server(port: Number): Server {
    return new Server(port);
  }

  start_server(callback: any): void {
    this.app.listen(this.port, callback);
    // this.public_source();
  }

  /*     
    Metodo para utilizar el server como render
    private public_source() {
            const publicPath = path.resolve(__dirname, '../public');
            this.app.use(express.static(publicPath));
            this.app.set('views', path.join(__dirname, 'views'));
            // configuracion de rutas de archivos de vistas 
            this.app.set('.hbs', expres_handlebars({
                extname: '.hbs',
                layoutsDir: path.join(this.app.get('views'), 'layouts'),
                partialsDir: path.join(this.app.get('views'), 'partial'),
                helpers: require('../lib/helpers')
            }));
    
            this.app.set('view engine', '.hbs');
    
            this.app.use(express.static(path.join(__dirname, 'public')))
        }
     */
}
